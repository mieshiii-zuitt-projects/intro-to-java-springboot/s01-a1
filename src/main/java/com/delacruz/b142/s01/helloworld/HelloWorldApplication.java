package com.delacruz.b142.s01.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class HelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldApplication.class, args);
	}

	@GetMapping("api/hello")
	public String hello(){
		return String.format("Hello, world! Welcome to Java + Spring Boot.");
	}

	@GetMapping("api/hello-name")
	public String  helloName(@RequestParam(value = "name", defaultValue = "world") String name) {
		return String.format("Hello, %s! Welcome to Java + Spring Boot.", name);
	}
	//http://localhost:8080/api/your-name/<name_input>
	@GetMapping("api/your-name/{name}")
	public String yourName(@PathVariable("name") String name) {
		return String.format("Hello, %S! How are you doing today?", name);
	}

	@GetMapping("api/goodEvening")
	public String goodEvening() {
		return String.format("Good evening");
	}
	//Activity solution
	@GetMapping("hi/{user}")
	public String hi(@PathVariable("user") String user){
		return String.format("Hi %s!!", user);
	}
}
